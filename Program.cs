﻿using System;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Text;
using System.Net;
using System.Collections.Generic;
using System.Numerics;

namespace WebIOStream
{
    class Program
    {
        public static string GetTextFromWeb(string adress)
        {
            
            System.Net.WebClient web = new System.Net.WebClient();
            string file = System.IO.Path.GetTempFileName();
            web.DownloadFile(adress, file);         
            var text = File.ReadLines(file).Where(n => n.Contains("@")).Where(n => !n.Contains("@media"));
            web.Dispose();
            return string.Join("\n", text);
                                                                               
        }
        
        public static string GetEmailFromWeb(string adress)
        {
            string emails = "";

            WebClient Client = new WebClient();
            Stream strm = Client.OpenRead(adress);
            StreamReader sr = new StreamReader(strm);
            string htmlFile;
            htmlFile = sr.ReadToEnd();
            strm.Close();

            Regex exp = new Regex("\\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}\\b", RegexOptions.IgnoreCase);
            MatchCollection matchCollection = exp.Matches(htmlFile);
            foreach (Match m in matchCollection)
            {
                emails += m + "\n";
            }

            return emails;
        }

        static void Main(string[] args)
        {
            try
            {
                Console.WriteLine("Podaj adress strony");
                string text = GetEmailFromWeb(Console.ReadLine());
                Console.WriteLine(text);
            }
            catch(Exception error)
            {
                Console.WriteLine(error);
            }
            
        }
    }
}
